# Cesar Breaker
Break the Cesar encription. 

# Requirements

* PyQt5 : Execute `pip3 install PyQt5` at the terminal.

# License

This project is under the license  [GPLv3](http://www.gnu.org/licenses/gpl.html). Read the COPYING.txt file for more information.

![GPL Image](http://www.gnu.org/graphics/gplv3-127x51.png)

## Recomended Links

* [Free Software Foundation](https://fsf.org) : https://fsf.org
* [GNU Project](https://gnu.org) : https://gnu.org
