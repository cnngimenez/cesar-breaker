# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'BreakWidget.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BreakerWidget(object):
    def setupUi(self, BreakerWidget):
        BreakerWidget.setObjectName("BreakerWidget")
        BreakerWidget.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(BreakerWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.le_input = QtWidgets.QLineEdit(BreakerWidget)
        self.le_input.setObjectName("le_input")
        self.horizontalLayout.addWidget(self.le_input)
        self.pb_decrypt = QtWidgets.QPushButton(BreakerWidget)
        self.pb_decrypt.setObjectName("pb_decrypt")
        self.horizontalLayout.addWidget(self.pb_decrypt)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.lw_decryptions = QtWidgets.QListWidget(BreakerWidget)
        self.lw_decryptions.setObjectName("lw_decryptions")
        self.verticalLayout.addWidget(self.lw_decryptions)

        self.retranslateUi(BreakerWidget)
        QtCore.QMetaObject.connectSlotsByName(BreakerWidget)

    def retranslateUi(self, BreakerWidget):
        _translate = QtCore.QCoreApplication.translate
        BreakerWidget.setWindowTitle(_translate("BreakerWidget", "Break Cesar"))
        self.pb_decrypt.setText(_translate("BreakerWidget", "Descifrar"))
