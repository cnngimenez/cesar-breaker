# -*- coding: utf-8 -*-
'''
Breaker Widget

A Qt Widget to show how to break the Cesar code. It allows to enter a plain
text and to decrypt it using all the possible passwords.

:copyright: 2019 Christian Gimenez
:author: Christian Gimenez
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# BreakerWidget.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PyQt5.QtWidgets import QWidget
from ui_breaker_widget import Ui_BreakerWidget
import cesar


class BreakerWidget(QWidget, Ui_BreakerWidget):
    def __init__(self, *args, **kargs):
        QWidget.__init__(self)
        self.setupUi(self)

        self.pb_decrypt.clicked.connect(self._on_pb_decrypt_clicked)

    def _on_pb_decrypt_clicked(self):
        """
        What to do when the user clicks on the Decrypt button.
        """
        self.lw_decryptions.clear()

        txt = self.le_input.text()
        lst_decripted = [(i, cesar.decrypt(i, txt)) for i in range(0, 26)]

        for elt in lst_decripted:
            self.lw_decryptions.addItem(str(elt[0]) + ": " + elt[1])
