#! /usr/bin/python3
# -*- coding: utf-8 -*-
'''
Main script.

Launch the Cesar Breaker.

:copyright: 2019 Christian Gimenez
:author: Christian Gimenez
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# main.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QApplication
from breaker_widget import BreakerWidget


if __name__ == "__main__":
    app = QApplication([])
    window = BreakerWidget()
    window.show()
    app.exec_()
