# -*- coding: utf-8 -*-
'''
Cesar Cipher Implementation.

Cesar cipher implementation.

:copyright: 2019 Candelaria Alvarez and Christian Gimenez
:author: Candelaria Alvarez and Christian Gimenez
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# cesar.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def encrypt(k: int, texto_plano: str):
    minusc = "abcdefghijklmnñopqrstuvwxyz"
    mayusc = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    cant = len(minusc)

    texto_cifrado = ""

    for c in texto_plano:
        if c in minusc:
            print("c: "+c)
            texto_cifrado += minusc[(minusc.index(c)+k) % cant]
        elif c in mayusc:
            texto_cifrado += mayusc[(mayusc.index(c)+k) % cant]
        else:
            texto_cifrado += c

    return texto_cifrado


def decrypt(k: int, texto_cifrado: str):
    minusc = "abcdefghijklmnñopqrstuvwxyz"
    mayusc = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    cant = len(minusc)

    texto_plano = texto_cifrado
    texto_cifrado = ""

    for c in texto_plano:
        print("c: "+c)
        if c in minusc:
            texto_cifrado += minusc[(minusc.index(c)-k) % cant]
        elif c in mayusc:
            texto_cifrado += mayusc[(mayusc.index(c)-k) % cant]
        else:
            texto_cifrado += c

    return texto_cifrado
